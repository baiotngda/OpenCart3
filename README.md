# OpenCart 3 中文免费版

OpenCart 3 中文免费版基于 OpenCart v3.0 开发。

## OpenCart 介绍
OpenCart is a free open source ecommerce platform for online merchants. OpenCart provides a professional and reliable foundation from which to build a successful online store.

## 安装
1. 代码 pull 或下载至本地
2. 执行 composer install
3. 执行 install 安装
